## A browser Hangman Game developed with JS.

- The game uses a local Json file with all the possible words;
- Every round a word is randomly picked;
- To guess a letter the user needs to press it on the keyboard;
- If the user get 6 letters wrong, it's game over;
- Winning or losing generates a pop up with the proper message and a play again button.

<img src="images/Hangman_Screenshot.jpg" width="800">